
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Students</title>
</head>
<body>
    <a href="{{ route('create') }}">create new</a>
    <table border>
        <thead>
            <th>Sl</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Date of Borth</th>
        </thead>
        <tbody>

            @foreach ($students as $stu)
                <tr>
                    <td>{{ $stu->id }}</td>
                    <td>{{ $stu->name }}</td>
                    <td>{{ $stu->phone }}</td>
                    <td>{{ $stu->dob }}</td>
                </tr>
            @endforeach

        </tbody>
    </table>
</body>
</html>