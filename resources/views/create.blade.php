<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create New Student</title>
</head>
<body>
    <form action="{{ route('store') }}" method="POST">
        @csrf
        <input type="text" name="name" placeholder="Full Name"><br>
        <input type="text" name="phone" placeholder="Phone Number"><br>
        <input type="date" name="dob">
        <input type="submit" value="Submit">
    </form>
</body>
</html>